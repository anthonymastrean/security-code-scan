package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/pathfilter"
	"gitlab.com/gitlab-org/security-products/analyzers/security-code-scan/v2/project"
)

const (
	nugetCmd                    = "nuget"
	msbuildCmd                  = "msbuild"
	dotnetCmd                   = "dotnet"
	pkgSecurityCodeScan         = "SecurityCodeScan"
	flagSecurityCodeScanVersion = "security-code-scan-version"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:    flagSecurityCodeScanVersion,
			Usage:   "Version of SecurityCodeScan",
			EnvVars: []string{"SECURITY_CODE_SCAN_VERSION"},
		},
	}
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	var ioArr []io.Reader

	filter, err := pathfilter.NewFilter(c)
	if err != nil {
		return nil, err
	}

	projects, err := findProjects(path, filter)
	if err != nil {
		return nil, err
	}

	for _, projectPath := range projects {
		// Note: since analyzeAll is set to true, `path` is the project root
		analyzeIo, err := analyzeProject(c, projectPath, path)
		if err != nil {
			return nil, err
		}
		ioArr = append(ioArr, analyzeIo)
	}

	return ioutil.NopCloser(io.MultiReader(ioArr...)), nil
}

// analyzeProject accepts a cli context and projectPath. The projectPath is the path to the directory
// containing a `.csproj` or `.vbproj` file. Using this path, analyzeProject will attempt to add, clean, and build the project
// using dotnet. If unable to "add" the project, msbuild and nuget will be used to restore and clean/build the project.
func analyzeProject(c *cli.Context, projectPath, projectRoot string) (io.Reader, error) {
	projectDir := filepath.Dir(projectPath)
	args := []string{"add", projectDir, "package", pkgSecurityCodeScan}

	// Add SecurityCodeScan package
	if c.IsSet(flagSecurityCodeScanVersion) {
		args = append(args, "-v", c.String(flagSecurityCodeScanVersion))
	}

	// Attempt to add project using dotnet
	out, err := run(dotnetCmd, projectDir, args, false)
	if err != nil {
		// if dotnetCmd fails, we will try building using msbuild and restoring using nuget
		log.Errorf("Error:\n%s", out)
		log.Info("dotnet command failed, trying msbuild")
		if err := project.InsertAnalyzersToProjFile(projectPath); err != nil {
			return nil, err
		}

		// nuget expects a solution file, so we need to find the parent sln file to the project
		slnDir, err := findSolution(projectDir, projectRoot)
		if err != nil {
			return nil, err
		}
		// restore using nuget
		out, err = run(nugetCmd, slnDir, []string{"restore"}, false)
		if err != nil {
			return nil, fmt.Errorf("Error %s on nuget restore:\n%s", err, out)
		}

		// build using msbuild
		out, err = run(msbuildCmd, projectDir, []string{projectDir, "-t:Clean;Build", "-consoleLoggerParameters:NoSummary;Verbosity=minimal"}, false)
		return bytes.NewBuffer(out), err
	}

	// clean project using dotnet
	out, err = run(dotnetCmd, projectDir, []string{"clean"}, false)
	if err != nil {
		log.Errorf("Error on dotnet clean:\n%s", out)
		return nil, err
	}

	// build using dotnet
	out, err = run(dotnetCmd, projectDir, []string{"build"}, true)
	if err != nil {
		// Ignore exit error
		if _, isExitErr := err.(*exec.ExitError); !isExitErr {
			return nil, err
		}
	}
	return bytes.NewBuffer(out), nil
}

// findSolution iterates through files, checks if there is a solution file in the current
// directory (projectDir), if not, recurse up the directory until at project root
func findSolution(projectDir, projectRoot string) (string, error) {
	parent := filepath.Dir(projectDir)
	files, err := ioutil.ReadDir(parent)
	if err != nil {
		return "", err
	}
	for _, f := range files {
		if filepath.Ext(f.Name()) == ".sln" {
			return parent, nil
		}
	}

	if projectDir == projectRoot {
		return "", fmt.Errorf("no .sln present in project")
	}

	return findSolution(parent, projectRoot)
}

func run(command string, dir string, args []string, errOut bool) ([]byte, error) {
	cmd := exec.Command(command, args...)
	cmd.Env = os.Environ()
	cmd.Dir = dir
	if errOut {
		cmd.Stderr = os.Stderr
		return cmd.Output()
	}
	return cmd.CombinedOutput()
}

func findProjects(dir string, filter *pathfilter.Filter) ([]string, error) {
	var projects []string

	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		relPath, err := filepath.Rel(dir, path)
		if err != nil {
			return err
		}

		if filter.IsExcluded(relPath) {
			if info.IsDir() {
				return filepath.SkipDir
			}

			return nil
		}

		ext := filepath.Ext(path)
		if ext == ".csproj" || ext == ".vbproj" {
			pathDir := filepath.Dir(path)
			if pathDir != dir {
				log.Infof("Found project in %s", filepath.Dir(path))
			}
			projects = append(projects, path)
			return filepath.SkipDir
		}
		return nil
	})

	return projects, err
}
